use std::fs::read_link;
use std::process::ExitCode;
use std::thread::sleep;
use std::time::Duration;

use nix::errno::Errno::{EPERM, ESRCH};
use nix::sys::signal::{kill, SIGTERM, SIGINT, SIGHUP};
use nix::unistd::Pid;
use notify_rust::Notification;
use swayipc::Connection;


fn notify() -> Notification {
	Notification::new()
		.summary("swaykill")
		.appname("swaykill").to_owned()
}

fn main() -> ExitCode {
	let pid = match Connection::new() {
		Err(_) => {
			notify().body("Failed to connect to sway.").show().unwrap();
			return ExitCode::FAILURE;
		},
		Ok(mut c) => match c.get_tree() {
			Err(_) => {
				notify().body("Failed to get client tree from sway.").show().unwrap();
				return ExitCode::FAILURE;
			},
			Ok(t) => match t.find_focused_as_ref(|n| { n.pid.is_some() }) {
				None => {
					notify().body("Failed to find active window.").show().unwrap();
					return ExitCode::FAILURE;
				},
				Some(n) => match n.pid {
					None => unreachable!(),
					Some(p) => Pid::from_raw(p)
				}
			}
		}
	};
	let exe = match read_link(format!("/proc/{pid}/exe")) {
		Ok(p) => p.to_str().unwrap().to_owned(),
		Err(e) => panic!("pid without /proc/pid/exe? {e}")
	};

	let mut should_kill = false;

	notify().body(format!("Found process with id {pid}.\nExecutable: {exe}").as_str())
		.action("kill", "Kill")
		.action("cancel", "Cancel")
		.show().unwrap()
		.wait_for_action(|action| match action {
			"kill" => {
				if cfg!(debug_assertions) {
					println!("kill! {}", pid);
				} else {
					should_kill = true;
				}
			},
			_ => ()
		});

	if should_kill {
		for sig in [SIGTERM, SIGINT, SIGHUP] {
			match kill(pid, sig) {
				Err(EPERM) => {
					notify().body("Permission denied.").show().unwrap();
					return ExitCode::FAILURE;
				},
				Err(ESRCH) => {
					notify().body("Process killed.").show().unwrap();
					return ExitCode::SUCCESS;
				},
				Err(e) => unreachable!("errno: {}", e.desc()),
				Ok(_) => match kill(pid, None) {
					Err(_) => {
						notify().body("Process killed.").show().unwrap();
						return ExitCode::SUCCESS;
					},
					Ok(_) => {
						if sig == SIGHUP {
							notify().body(format!("Failed to kill process with id {pid}.").as_str()).show().unwrap();
							return ExitCode::FAILURE;
						} else {
							sleep(Duration::from_secs(1));
						}
					}
				}
			}
		}
	}

	ExitCode::SUCCESS
}
